/**
 * \file hs_cache.h
 * \brief Header file for hs_cache.c
 **/

#ifndef TOR_HS_PIRPROCESS_H
#define TOR_HS_PIRPROCESS_H

#define PIRPROCESS_HDR_SIZE 13

#include "lib/container/buffers.h"

typedef void (*pir_process_msghandler_t)(const unsigned char *hdrbuf,
        const char *bodybuf, size_t bodylen);

typedef struct pir_process_st *pir_process_t;
typedef void (*pir_process_abort_fn)(dir_connection_t *);

void hs_pirprocess_poke(const char *path, const char *loglabel,
    pir_process_msghandler_t msghandler, pir_process_t *handlep);

void hs_pirprocess_close(pir_process_t *handlep);

int hs_pirprocess_send(pir_process_t handle, const unsigned char *buf,
    size_t len);

void hs_pirprocess_init(void);
void hs_pirprocess_abort_all(void);
void hs_pirprocess_free_all(void);
uint64_t hs_pirprocess_alloc_reqid(dir_connection_t *dir_conn,
        pir_process_abort_fn abort_fn);
void hs_pirprocess_dealloc_reqid(dir_connection_t *dir_conn);
dir_connection_t *hs_pirprocess_lookup_reqid(uint64_t reqid);

#endif /* !defined(TOR_HS_PIRPROCESS_H) */
